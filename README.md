# Polkadot Node Deployment
This repo contains resources related to deployment of **Polkadot** node (*full node* or *archive node*) which can connect to various networks such as **Polkadot**, **Kusama** or **Westend** etc. This node can be deployed in many ways e.g. as systemd service on linux server or as pod on kubernetes cluster. This repo relates to deployment of node on kubernetes cluster. If you wish to deploy node as service on linux/windows/macOS then please refer to following links which directs to official documentations explaining the steps:
- [Polkadot github repo](https://github.com/paritytech/polkadot)
- [Set up Full Node](https://wiki.polkadot.network/docs/maintain-sync)
- [Monitor your node](https://wiki.polkadot.network/docs/maintain-guides-how-to-monitor-your-node)   

**Note:** Please note that deploying node without downloading database snapshot would take hours to sync up with the network depending upon your server configurations so I would suggest to first download the snapshot of network node you wish to deploy from respective url: [Polkadot](https://dot-rocksdb.polkashots.io/) or [Kusama](https://ksm-rocksdb.polkashots.io/).   

![polkadot](polkadot-node.png)   

Major topics covered in this repo are as follows:
- [Infrastructure setup for node deployment](#infrastructure-setup-for-node-deployment)
- [Node Deployment](#node-deployment)
- [Setting up monitoring for the node](#setting-up-monitoring-for-the-node)
- [Explaination about Pipeline](#explaination-about-pipeline)   
- [Scope of Improvment](#scope-of-improvement)

## Infrastructure setup for node deployment
We first need infrastructure which includes kubernetes cluster for deployment of the node. Directory `infra-terraform` within this repo contains terraform code which can be used to create resources on AWS cloud platform and includes resources such as VPC, EKS (Kubernetes cluster), Networking etc. If you wish to use this terraform code and understand how to use it then please refer following link:
- [Infrastructure setup details](infra-terraform)   

## Node Deployment
This repo explains deployment of polkadot node on kubernetes cluster with the help of helm charts. These helm charts can be deployed on kubernetes platform either from ci/cd pipeline or from your local pc. Pipeline stage has been added in the `.gitlab-ci.yml` file within this repo for deployement of node using helm charts. Details about deploying kubernetes resources has been explained in node directory so please refer following link:
- [Deployment of Node](node)

## Setting up monitoring for the node
There are many tools and technology stacks are available for monitoring the resources. We have considered **Prometheus** stack for monitoring our resources. We can deploy prometheus stack on kubernetes cluster in couple of ways e.g. by configure manifest files manually or by using prometheus operator.   
- [Manual Configuration](monitoring): Using this method, you need to configure and manage resources yourself. You create manifest files for deploying prometheus stack on kubernetes cluster which will run a pod with prometheus binary image. You can make use of configmap resource to pass configuration to prometheus such as `scrape_config`. You can refer to [**monitoring**](monitoring) directory for more details and manifest files.   
- [Prometheus Operator](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack): This is more efficient way of deploying and managing prometheus stack on kubernetes cluster using helm charts. You can install helm charts using following commands:   
```
## Add helm repo
$ helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
$ helm repo update

## Install chart
$ helm install [RELEASE_NAME] prometheus-community/kube-prometheus-stack
```
This will create resources such as StatefulSets, Services, CustomResourceDefinitions, ConfigMaps, Secrets, ServiceAccounts, Roles, RoleBindings for prometheus, grafana and node exporters etc. By default, node exporter fetches node metrics e.g. cpu, memory etc. and prometheus will scrape these metrics and can be viewed on grafana dashboard. To access prometheus or grafana ui temporarily, you can use `kubectl port-forward` as by default these are exposed within cluster only using headless and clusterIP services. To access grafana ui through public internet then expose it either by exposing it as nodeport service or by implementing ingress resources on the cluster.   

Prometheus operator uses kubernetes object **ServiceMonitor**, which is created as custom resource definition, to monitor third party applications, polkadot in our case. This servicemonitor resource will help in service discovery of various services such as polkadot node service, which exposes substrate metrics, with the help of labels. Once configured polkadot node metrics can be viewed on grafana dashboard. You can refer node directory within this repo for servicemonitor manifest file and details.   

A stage `prometheus-operator-deploy` in ci/cd pipeline file `.gitlab-ci.yml` has also been configured to deploy prometheus operator on kubernetes cluster. This stage can only be triggered by passing key-value pair as variables while running the pipeline. Key is 'MONITORING' and value should be 'prometheus'.

**Note:** Please note that names of all metrics exported by polkadot node start with `substrate_` e.g. `substrate_block_height`, this is important while importing and existing grafana dashboard from marketplace. If you are importing any existing dashboard such as `My Polkadot Metrics` with ID: 12425 then you may not see these metrics as this dashboard looks for metrics whose name start with `polkadot_` e.g. `polkadot_block_height`. You can download `json` file of the dashboard and look for targets.

## Explaination about Pipeline
This repo contains gitlab CI/CD pipeline file `.gitlab-ci.yml` which also includes another file `terraform.gitlab-ci.yml` in infra-terraform directory which contains stages related to creation of aws resources using terraform.
- **Terraform Stages:** `terraform.gitlab-ci.yml` file in infra-terraform directory are: *Validate* which will validate the terraform code; *Plan* which will show a preview of aws resources that will be created or updated; *Apply* will implement or create the resources. These stages are configured in such a way that these will only be triggered if any change is made in `infra-terraform` directory only. Any change anywhere else in this repo won't trigger these stages. Apply stage needs to be triggered manually as well.   
- **Node Deployment Stage:** In `.gitlab-ci.yml` file stage `node-deploy` is configured to deploy polkadot node on kubernetes cluster. This stage will only be triggered if any change is made within `node` directory within this repo. As of now, two parameters (`node.name` and `node.chain`) are passed in the pipeline using `--set` flag.
- **Monitoring:** For monitoring there are two stages, one for deploying prometheus operator and another for deploying prometheus with manual configurations. Prometheus operator stage can be triggered by passing variables (MONITORING:prometheus) while running pipeline. Prometheus with manual configurations is triggered when any change is made in `monitoring` directory. You can run either of these stages depending upon which solution you chose to deploy prometheus in kubernetes cluster.   

## Scope of Improvement
There is significant scope of improvement in this repo, which could not be covered yet due to time limitation, and I would love to pursue and look into and improve over the time. I'm outlining some points here which can be worked upon:
- **Release strategy:** A proper release strategy can be implemented with the help of branches and tags. Separate branches can be created for different purposes along with tags. As of now everything is done in main branch only.
- **Linting and testing:** Linting and testing of code can be implemented such as helm code linting.
- **Security:** Security of the setup can be improvised with the help of various AWS and kubernetes functionalities. We can limit public access of the service (say grafana dashboard) with the help of AWS firewalls such as securitygroups and NACL and limit access to particular ports only. We can put a load balancer in front and implement SSL/TLS certificate on it. We can make use of kubernetes namespaces, network policy, service account and RBAC etc.
- **Code:** Code can be made more generic be it terraform code or helm code. Currently, there are some values which are hardcoded within the code and can be made more generic so that these can be passed at the time of creation/deployment as per users' requirements. Add more comments to the code so that it can be self explanatory.
- **Health Checks:** Health checks can be implemented on all services running in the cluster.