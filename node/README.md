# Polkadot node deployment
This directory contains helm chart for deploying polkadot node. This chart includes manifest files creating kubernetes resources such as statefulsets, service and servicemonitor. If you wish to deploy this on your own kubernetes cluster then you can clone this repo and run following commands:
```
cd node/
helm install <release-name> . --set node.name=my-polkadot-node
```
This will deploy node which connects to kusama network by default. It will create following resources on kubernetes cluster:
- **Statefulset:** This statefulset will create a pod with *init container* which will download **kusama database snapshot**, as of now snapshot url is hardcoded and will be customized so that **polkadot database snapshot** can also be downloaded just by passing a parameter. Once init container is completed then main container will run with latest version of polkadot binary image. Manifest file `statefulset.yaml` has also been configured to create *persistent volume claim* with specified storage requirement which will create *persistent volume* dynamically using default *storage class*, which is *gp2* in our case (AWS EBS).   
- **Service:** Manifest file `service.yaml` will create a *headless service* resource which will expose pod metrics on port `9615` and can be further exported to monitoring tools such as prometheus.   
- **Servicemonitor:** This is a custom resource and would need *customresourcedefinition* created on the cluster already. This crd will be created as a part of prometheus stack deployment using prometheus operator. This resource helps prometheus in service discovery of the service it is mapped to with the help of labels.   

You can pass values of parameters through `values.yaml` file or using `--set` flag in pipeline e.g. `--set node.name` or `--set node.chain`.   

Once the node is deployed, you should be able to see your node on [Polkadot telemetry interface](https://telemetry.polkadot.io/#list/0xb0a8d493285c2df73290dfb7e61f870f17b41801197a149ca93654499ea3dafe). Click anywhere on the screen and type your node name e.g. my-polkadot-node.   

If you wish **not** to deploy prometheus stack then you don't need to deploy *servicemonitor* resource so for doing that you need to set `prometheus.operator.enabled` parameter as `false` in `values.yaml` file. Otherwise you need to first deploy *customresourcedefinition* resource on kubernetes cluster before deploying *servicemonitor*.   

**Note:** As of now, these resources will be deployed in default namespace. We can make this set up more secure by configuring various kubernetes functionalities such as **Network Policy**, **ServiceAccount**, **RBAC** etc.