# AWS Infra resources using Terraform
This directory contains a set of modules for creating AWS resources using Terraform so that application can be deployed on kubernetes cluster. These modules include following resources:
- VPC
- EKS (Kubernetes) Cluster
- EKS worker node group
- Security Groups along with rules
- Load Balancer, target group and listener rules
- EC2 instance as bastion/jump host
- IAM roles

## Elastic Kubernetes Cluster
Amazon Elastic Kubernetes Service (Amazon EKS) is a managed container service to run and scale Kubernetes applications in the cloud or on-premises. Kubernetes is an open-source container orchestration system for automating software deployment, scaling, and management.

## To Create AWS resources using CI/CD:
1. `git clone` this repo to your computer.
2. Make changes as per your need. You can add or remove modules/resources according to the requirements.
3. Install Terraform.
4. Values to various parameters e.g. CIDR is passed in `terraform.tfvars` file. You can pass your own values or leave it as is. Please change bucket name and key for state file in `main.tf` file.
5. Terraform needs AWS access key and secret key to create resources. These keys can be passed in terraform.tfvars file but this is not secure method. These keys can also be passed through gitlab secret variables so create these two gitlab variables with respective values:
> TF_VAR_AWS_ACCESS_KEY_ID   
> TF_VAR_AWS_SECRET_ACCESS_KEY   
6. Add changes by runnig `git add` and specifying files.
7. Commit your changes by running `git commit -m "Description"`
8. Push the code to the repo by running `git push -u origin main` after committing the changes.
9. This will trigger the CI/CD pipeline, as pipeline file `terraform.gitlab-ci.yml` has been added to the repo. First it will initialize and validate the code. After initialiazing terraform, it will run plan command which will show resources it will create. Finally it will run `terraform apply` command which will create the infrastructure, this stage/job needs to be triggered manually as configured in the pipeline.   
**Note:** Please note that we are passing variable file `terraform.tfvars` so we have to add `-var=terraform.tfvars` with terraform plan and apply commands.

## To Create AWS resources from your PC:
1. `git clone` this repo to your computer.
2. Make changes as per your need. You can add or remove modules according to the requirements.
3. Install Terraform.
4. Values to various parameters e.g. CIDR is passed in `terraform.tfvars` file. You can pass your own values or leave it as is.
5. Terraform needs AWS access key and secret key to create resources. These keys can be passed in terraform.tfvars file but this is not secure method. These keys can also be passed through environment variables so configure these two environment variables with respective values:
> TF_VAR_AWS_ACCESS_KEY_ID   
> TF_VAR_AWS_SECRET_ACCESS_KEY   
6. Run `terraform init` to initialize providers.
7. Run `terraform validate` to validate the code, it would throw an error if there is any syntax error.
8. Run `terraform plan` to check which resources will be created.
9. Run `terraform apply`. It would ask for yes or no, please type yes and press Enter.   

Once resources have been created and are in active state then generate kubeconfig for accessing kubernetes cluster.