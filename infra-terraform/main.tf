terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket = "polkadot-node-tf-state"
    key = "parity/terraform.tfstate"
    region = "ap-south-1"
  }
}

provider "aws" {
  access_key = var.AWS_ACCESS_KEY_ID
  secret_key = var.AWS_SECRET_ACCESS_KEY
  region     = var.AWS_DEFAULT_REGION
}

#### VPC Module call
module "aws-vpc" {
  source = "./modules/aws-vpc"

  aws_cluster_name         = var.aws_cluster_name
  aws_vpc_cidr_block       = var.aws_vpc_cidr_block
  aws_avail_zones          = slice(data.aws_availability_zones.available.names, 0, 3)
  aws_cidr_subnets_private = var.aws_cidr_subnets_private
  aws_cidr_subnets_public  = var.aws_cidr_subnets_public
  default_tags      = var.default_tags
  eks_cluster_sg_id = module.aws-eks-cluster.eks_cluster_sg_id
}

#### Bastion EC2 Instance
resource "aws_instance" "bastion-server" {
  ami           = data.aws_ami.distro.id
  instance_type = var.aws_bastion_size
  count                       = 1
  associate_public_ip_address = true
  subnet_id              = element(slice(module.aws-vpc.aws_subnet_ids_public, 0, 2), count.index)
  vpc_security_group_ids = ["${module.aws-vpc.aws_bastion_sg}"]
  key_name               = "polkadot"

  credit_specification {
    cpu_credits = "standard"
  }
}
