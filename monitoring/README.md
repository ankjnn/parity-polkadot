## Setting up monitoring for the node
This directory contains helm charts/kubernetes manifest files for deploying prometheus on kubernetes cluster. This will deploy resources such as statefulsets, services, configmaps, secrets etc. for prometheus and grafana.   
Please clone this repo and run following command to deploy:
```
## Clone the repo
$ git clone
$ cd monitoring
## Helm install
$ helm install [release-name] .
```
To pass configurations to prometheus such as `scrape_config` to register various targets to prometheus, there is configmap manifest file `prom-cm.yaml` which maps `prometheus.yaml` file containing configurations to prometheus pod at `/etc/config` location within pod and can be used by prometheus binary. Currently two targets are configured, one is prometheus localhost:9090 to monitor itself and other one is using prometheus service discovery functionality for kubernetes using `kubernetes_sd_configs` keyword for monitoring polkadot service with the help of labels.